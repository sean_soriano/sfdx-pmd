Install Docker for Windows
- https://www.docker.com/docker-windows

start docker for windows from Desktop Program
- login when docker starts.. (may take some time)

build docker container
> docker build -t davanti/salesforcedx .
push to latest version
> docker push davanti/salesforcedx

Push tagged version
> docker tag salesforce davanti/salesforcedx:{version}
> docker push davanti/salesforcedx:{version}

check images
> docker images
remove images
> docker -f rmi <imageid>

Check on Docker Hub
Login 
- https://hub.docker.com/
- search davanti/salesforcedx
