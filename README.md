# PMD

https://pmd.github.io/latest/index.html

## Components 
1. **build** - contains pmd library.
2. **ruleset** - A ruleset is an XML configuration file, which describes a collection of rules to be executed in a PMD run. Uses standard Apex Rulesets provided by PMD(https://pmd.github.io/latest/pmd_rules_apex.html)

## Usage:

Run from cli

`build/pmd/bin/run.sh pmd -dir force-app/ -format text -language apex -rulesets apex-ruleset.xml -cache build/pmd.cache`

## Options
https://pmd.github.io/pmd-6.0.1/pmd_userdocs_getting_started.html#options

Command Options      |Description
--------------|-----------
 `-cache` | Specify a location for the analysis cache file to use. This can greatly improve analysis performance and is highly recommended.
 `-dir` | Root directory for sources to be scanned
 `-format`  | Report format type. Default format is 'text'. Other report formats: https://pmd.github.io/pmd-6.0.1/pmd_userdocs_getting_started.html#available-report-formats--renderers
 `-language` | Specify a language PMD should use. `apex` for Salesforce
 `-rulesets` | Comma separated list of ruleset names to use. 


