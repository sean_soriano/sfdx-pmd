/**
 * Created by heyoo on 4/11/18.
 */
public with sharing class Objects {
    public static Map<String, String> getParams(Object obj, String prefix, Map<String, String> replacements) {
        if(obj == null){
            return null;
        }
        if(prefix == null){
            prefix = '';
        }
        Map<String, String> parameters = new Map<String, String>();
        Map<String, Object> o = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(obj));
        for (String i : o.keySet()) {
            Object val = o.get(i);
            if (val != null) {
                if (replacements != null && replacements.containsKey(i)) {
                    i = replacements.get(i);
                }
                parameters.put(prefix + i, String.valueOf(val));
            }
        }
        return parameters;
    }
}